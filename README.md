# SolArc Theme
SolArc is a flat theme with transparent elements for GTK 3, GTK 2 and GNOME Shell which supports GTK 3 and GTK 2 based desktop environments like GNOME, Unity, Pantheon, Xfce, MATE, Cinnamon (>=3.4), Budgie Desktop (10.4 for GTK+3.22) etc.

SolArc is a fork of the Arc theme: https://github.com/jnsh/arc-theme

Colours are based on the Solarized colour scheme by [Ethan Schoonover](https://github.com/altercation): https://github.com/altercation/solarized

Most of the work is done inside [solarize.sh](./solarize.sh), which replaces various Arc colours with matching Solarized colours.

## SolArc is available in three variants
### SolArc
![A screenshot of the Arc theme](https://github.com/schemar/solarc-theme/blob/master/screenshots/preview-light.png?raw=true)

### SolArc-Darker
![A screenshot of the Arc-Darker theme](https://github.com/schemar/solarc-theme/blob/master/screenshots/preview-darker.png?raw=true)

### SolArc-Dark
![A screenshot of the Arc-Dark theme](https://github.com/schemar/solarc-theme/blob/master/screenshots/preview-dark.png?raw=true)

## Installation

**Important:** Remove all older versions of the theme from your system before you proceed any further.

    sudo rm -rf /usr/share/themes/{SolArc,SolArc-Darker,SolArc-Dark}
    rm -rf ~/.local/share/themes/{SolArc,SolArc-Darker,SolArc-Dark}
    rm -rf ~/.themes/{SolArc,SolArc-Darker,SolArc-Dark}

To build the theme the following packages are required
* `git` to clone the source directory
* `wget` for pulling the Arc source
* `meson` for building Arc theme
* `sassc` for GTK 3, Cinnamon, or GNOME Shell
* `pkg-config` or `pkgconfig` for Fedora
* `optipng` for GTK 2, GTK 3, or XFWM
* `inkscape` for GTK 2, GTK 3, or XFWM

The following packages are optionally required
* `gnome-shell`for auto-detecting the GNOME Shell version
* `libgtk-3-dev` for Debian based distros or `gtk3-devel` for RPM based distros, for auto-detecting the GTK3 version

**Note:** For distributions which don't ship separate development packages, just the GTK 3 package is needed instead of the `-dev` packages.

For the theme to function properly, install the following
* GNOME Shell 3.18+, GTK 3.18+      (for gnome users only perhaps?)
* The `gnome-themes-extra` package  (for gnome users only perhaps?)
* The murrine engine. This has different names depending on the distro.
  * `gtk-engine-murrine` (Arch Linux)
  * `gtk2-engines-murrine` (Debian, Ubuntu, elementary OS)
  * `gtk-murrine-engine` (Fedora)
  * `gtk2-engine-murrine` (openSUSE)
  * `gtk-engines-murrine` (Gentoo)

WARNING: Make sure to check the Arc
[installation instructions](https://github.com/jnsh/arc-theme/blob/master/INSTALL.md)
to make sure you have all the dependencies you need as those might change without
me knowing it.

Install the theme with the following commands

**1. Get the source**

If you want to install the latest version from git, clone the repository with

    git clone https://gitlab.com/Night_H4nter/solarc-gtk-theme && cd solarc-theme

**2. Download & patch the Arc theme source**

    ./solarize.sh

**3. Build and install the patched theme**

For example:

    cd arc-theme-[version]
    meson setup -Dthemes=gnome-shell,gtk2,gtk3,gtk4,metacity,plank,xfwm -Dvariants=dark --prefix=$HOME/theme build/
    meson install -C build/

This will build a dark variant for everything except Cinnamon and Unity with
transparency enabled and install it into `$HOME/.local/share/themes`.
Consult the 
[build instructions](https://github.com/jnsh/arc-theme/blob/master/INSTALL.md#building-and-installation)
for Arc theme on how to use `meson` flags to customize the resulting theme
to your needs.

After the installation is complete the theme can be activated with `gnome-tweak-tool` or a similar program by selecting `SolArc`, `SolArc-Darker` or `SolArc-Dark` as Window/GTK+ theme and `SolArc` or `SolArc-Dark` as GNOME Shell/Cinnamon theme.

If the `-Dtransparency=false` flag was set, the theme will be installed as `SolArc-solid`, `SolArc-Darker-solid` and `SolArc-Dark-solid`.

**Uninstall the theme**

Manually remove the theme directories from your install location, e.g.
    
    rm -rf ~/.local/share/themes/SolArc{,-Dark,-Darker,-Lighter}{,-solid}

## Troubleshooting
If you get artifacts like black or invisible backgrounds under Unity, disable overlay scrollbars with

    gsettings set com.canonical.desktop.interface scrollbar-mode normal

## Contributing and bug reporting
It's not like i'm a good maintainer, so no promises here whatsoever, but i might try help you/process your contribution if you create an issue.
